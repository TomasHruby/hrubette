#### Project setup:

##### Configs:
- Copy and update config files from `_examples/` directory
- Create final classes by using extends of abstract classes from rockette
- Prepare `composer.json` and run `update`
- Setup right access to SQL and run migrations or run it via Rockette Dev Tools
- Copy paste run-scripts from `_examples/composer.json` to main project composer and remeber to change `src` to `app`
- Copy and paste standards.xml into your root directory of your project and run code sniffer:

`"composer code:fix"` - auto fixing PSR standards by standard.xml from root directory PHPCBF

`"composer code:check"` - check code sniffer PHPCS
 
`"composer code:analyse"` - analyse code PHP STAN
   
 
##### Composer repo:

For local development of rockette create project directory `packages/rockette` inside of rockette-project-skeleton with symlink.

For local development of project based on rockette use symlink like this:
```
"repositories": [
    {
        "type": "path",
        "url": "c:/xampp-php806/htdocs/rockette-project-skeleton/packages/rockette",
        "symlink": true
    }
],
```

For production use:
``` 
"repositories": [
    {
        "type": "vcs",
        "url": "git@gitlab.com:TomasHruby/rockette.git"
    }
],
```

And `"tomashruby/rockette": "dev-master"` in main composer.json file.


##### Console:
All commands located at `Rockette/Console` must be extended by final classes in your projects (`app/Console`).
Than please register them as services in your app and remember to add them into `scripts` of `composer.json`.
For help see config-templates. 
