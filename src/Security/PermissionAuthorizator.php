<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */
declare(strict_types=1);

namespace Rockette\Security;

/**
 * @author  Tom Hruby
 * https://tomashruby.com
 * Class PermissionAuthorizator
 * @package Rockette\Security
 * Implements Nette/Security/Authorizator
 */
abstract class PermissionAuthorizator extends \Nette\Security\Permission
{

    /**
     * Only quest
     */
    const ROLE_GUEST = 'quest';

    /**
     * All done, user is verified and signed in
     */
    const ROLE_AUTHENTICATED = 'authenticated';

    /**
     * Not finished registration
     */
    const ROLE_ONBOARDING = 'onboarding';

    const PRIVILEGE_INSERT = 'insert';
    const PRIVILEGE_UPDATE = 'update';
    const PRIVILEGE_DELETE = 'delete';
    const PRIVILEGE_VIEW = 'view';

    public function __construct() {
        $this->addProjectRoles();
        $this->addProjectResources();
        $this->addPermissions();
    }

    /**
     * Example:
     * $this->addRole(static::ROLE_GUEST);
     * $this->addRole(static::ROLE_AUTHENTICATED);
     * $this->addRole(static::ROLE_ONBOARDING);
     */
    protected function addProjectRoles() {}

    /**
     * Example:
     * $this->addResource('account');
     */
    protected function addProjectResources() {}

    /**
     * Example:
     * $this->allow(static::ROLE_AUTHENTICATED, 'account', 'edit');
     * $this->deny(static::ROLE_AUTHENTICATED, 'account', 'delete');
     */
    protected function addPermissions() {}

}
