<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */

namespace Rockette\Security;

/**
 * @deprecated ONLY EXAMPLE
 * @author  Tom Hruby
 * https://tomashruby.com
 * Class AuthenticatorExample
 * @package Rockette\Security
 */
final class Authenticator implements \Nette\Security\Authenticator
{

    /**
     * @var \Rockette\Model\Repo\AccountRepo
     */
    protected $accountRepo;

    /**
     * @var \Rockette\Security\PasswordHasher
     */
    private $passwordHasher;

    /**
     * @var \Rockette\Security\PermissionAuthorizator
     */
    private $permission;

    /**
     * AuthenticatorExample constructor.
     *
     * @param \Rockette\Model\Repo\AccountRepo   $accountRepo
     * @param \Rockette\Security\PasswordHasher  $passwordHasher
     * @param \Rockette\Security\PermissionAuthorizator $permission
     */
    public function __construct(
        \Rockette\Model\Repo\AccountRepo $accountRepo,
        \Rockette\Security\PasswordHasher $passwordHasher,
        \Rockette\Security\PermissionAuthorizator $permission
    ) {
        $this->accountRepo = $accountRepo;
        $this->passwordHasher = $passwordHasher;
        $this->permission = $permission;
    }

    /**
     * @param  string $email
     * @return \Rockette\Model\Entity\AccountInterface
     * @throws \Rockette\Model\Exception\Runtime\EntityNotFound
     */
    public function getAccountByEmail(string $email): \Rockette\Model\Entity\AccountInterface {
        $account = $this->accountRepo->getSingleBy(['email' => $email]);
        if (!$account) {
            throw new \Rockette\Model\Exception\Runtime\EntityNotFound('User not found!');
        }
        return $account;
    }

    /**
     * @param string $email
     * @param string $password
     * @return \Nette\Security\IIdentity
     * @throws \Nette\Security\AuthenticationException
     */
    public function authenticate(string $email, string $password): \Nette\Security\IIdentity {
        try {
            $account = $this->getAccountByEmail($email);
        } catch (\Rockette\Model\Exception\Runtime\EntityNotFound $e) {
            throw new \Nette\Security\AuthenticationException('User not found: ' . $e->getMessage(), \Nette\Security\Authenticator::IDENTITY_NOT_FOUND);
        }

        if (!$account) {
            throw new \Nette\Security\AuthenticationException('User not found', \Nette\Security\Authenticator::IDENTITY_NOT_FOUND);
        } elseif (!$this->passwordHasher->verify($password, $account->password)) {
            throw new \Nette\Security\AuthenticationException('The password is incorrect.', \Nette\Security\Authenticator::INVALID_CREDENTIAL);
        } elseif ($this->passwordHasher->needsRehash($account->password)) {
            $account->password = $this->passwordHasher->hash($password);
            $this->accountRepo->persist($account);
        }

        $identity = new \Rockette\Security\BasicIdentity($account);

        return $identity;
    }

    public function passwordEncode(string $password): string {
        return $this->passwordHasher->hash($password);
    }

}
