<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */

namespace Rockette\Service;

abstract class ConfigProvider
{

    /**
     * Logged frontend user session expiration (for auto logout)
     */
    const FRONTEND_USER_EXPIRATION = 'frontendUserExpiration';

    /**
     * @var array
     */
    protected array $config;

    /**
     * ConfigProvider constructor.
     *
     * @param array $config
     */
    public function __construct(array $config) {
        $this->config = $config;
    }

    /**
     * @param  string $name
     * @return mixed
     */
    public function getParameter(string $name) {
        $this->config[$name];
    }

}
