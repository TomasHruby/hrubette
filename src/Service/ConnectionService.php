<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */

namespace Rockette\Service;

use Dibi\Connection;

abstract class ConnectionService implements ConnectionServiceInterface
{

    public Connection $connection;

    public function __construct(Connection $connection) {
        $this->connection = $connection;
    }

    /**
     * @return Connection
     */
    public function getConnection(): Connection {
        return $this->connection;
    }

    public function transactionBegin() {
        $this->connection->begin();
    }

    public function transactionCommit() {
        $this->connection->commit();
    }

    public function transactionRollback() {
        $this->connection->rollback();
    }

}
