<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */
declare(strict_types=1);

namespace Rockette;

class Application
{

    public function getVersion() {
        return '1.0';
    }

}
