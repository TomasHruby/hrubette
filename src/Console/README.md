1. Create final class in your application, eg: `app/Console` and inherit from abstract classes of Rockette/Console
2. Add `defaultName` property into final classes: 
`protected static $defaultName = 'schema-generator';`
3. Register app classes into `app/include/console.neon` file as a service
4. Add scripts into `composer.json` scripts commands, eg.: `"clean:log": "php bin/console clean log"` 
5. Run commands via terminal: `composer clean:log`
   

