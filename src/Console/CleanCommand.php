<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */
declare(strict_types=1);

namespace Rockette\Console;

use Exception;
use Nette\Utils\FileSystem;
use Nette\Utils\Finder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class CleanCommand extends Command
{

    const DEFAULT_DAYS = '- 5 days';

    const ZERO_DAYS = '0';

    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'clean';

    protected string $logsDir;

    protected string $tempDir;

    public function __construct(string $logsDir, string $tempDir) {
        parent::__construct();
        $this->logsDir = $logsDir;
        $this->tempDir = $tempDir;
    }

    protected function configure(): void {
        $this->setDescription('Clean old log and cache files from log directory');
        $this->addArgument('whatToClean', InputArgument::REQUIRED, 'What you want to clean? Type one of: log, logs, cache');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        try {

            $whatToClean = $input->getArgument('whatToClean');
            switch ($whatToClean) {
            case 'log':
                $paths = $this->getLogDir();
                break;
            case 'logs':
                $paths = $this->getLogsDirs();
                break;
            case 'cache':
                $paths = $this->getCacheDirs();
                break;
            default:
                throw new \InvalidArgumentException('Unknown whatToClean type.');
            }

            $output->writeln('Start cleaning...');

            foreach ($paths as $path => $cleanDate) {

                if (!is_dir($path)) {
                    $output->writeln('Skipping directory (Not a real path): ' . $path);
                    continue;
                }

                $pathsToDelete = [];
                $finderDir = Finder::findDirectories('*')->from($path)->date('<', $cleanDate);
                foreach ($finderDir as $dirPath => $dir) {
                    $pathsToDelete[] = $dirPath;
                }

                $finderFile = Finder::findFiles('*')->exclude('.git*')->from($path)->date('<', $cleanDate);

                foreach ($finderFile as $filePath => $file) {
                    $pathsToDelete[] = $filePath;
                }

                $output->writeln('Deleting ' . $finderDir->count() . 'x dirs and ' . $finderFile->count() . ' files in ' . $path);

                foreach ($pathsToDelete as $delete) {
                    FileSystem::delete($delete);
                }
            }

        } catch (Exception $ex) {
            $output->writeln('An error occured while trying to clean logs dir: ' . $ex->getMessage());
            return Command::FAILURE;
        }
        $output->writeln('End of cleaning.');
        return Command::SUCCESS;
    }

    /**
     * @return array
     */
    private function getLogDir() {
        return [
        $this->logsDir => self::ZERO_DAYS,
        ];
    }

    /**
     * @return array
     */
    private function getLogsDirs() {
        return [
        $this->logsDir . '/aha' => self::DEFAULT_DAYS,
        $this->logsDir . '/test' => self::ZERO_DAYS,
        ];
    }

    /**
     * @return string[]
     */
    private function getCacheDirs() {
        return [
        $this->tempDir . '/cache' => self::ZERO_DAYS,
        ];
    }

}
