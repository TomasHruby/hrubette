<?php
declare(strict_types=1);

namespace Rockette\Model\Mapper;

/**
 * Application specific mapper
 *
 * @author Tomáš Hrubý
 */
abstract class Mapper extends \LeanMapper\DefaultMapper implements MapperInterface
{

    const REPOSITORY_CLASS_NAME = 'Repo';

    /**
     * Model\Entity\SomeEntity -> some_entity
     *
     * @param  string $entityClass
     * @return string
     */
    public function getTable(string $entityClass): string {
        return $this->camelToUnderdash($this->trimNamespace($entityClass));
    }

    /**
     * Model\Repository\SomeEntityRepository -> some_entity
     *
     * @param  string $repositoryClass
     * @return string
     */
    public function getTableByRepositoryClass(string $repositoryClass): string {
        $class = preg_replace('#([a-z0-9]+)' . self::REPOSITORY_CLASS_NAME . '$#', '$1', $repositoryClass);
        return $this->camelToUnderdash($this->trimNamespace($class));
    }

    /**
     * someField -> some_field
     *
     * @param  string $entityClass
     * @param  string $field
     * @return string
     */
    public function getColumn(string $entityClass, string $field): string {
        return $this->camelToUnderdash($field);
    }

    /**
     * some_field -> someField
     *
     * @param  string $table
     * @param  string $column
     * @return string
     */
    public function getEntityField(string $table, string $column): string {
        return $this->underdashToCamel($column);
    }

    /**
     * some_entity -> Model\Entity\SomeEntity
     *
     * @param  string   $table
     * @param  \LeanMapper\Row|null $row
     * @return string
     */
    public function getEntityClass(string $table, ?\LeanMapper\Row $row = NULL): string {
        return $this->defaultEntityNamespace . '\\' . ucfirst($this->underdashToCamel($table));
    }

    /**
     * camelCase -> underdash_separated.
     *
     * @param  string
     * @return string
     */
    protected function camelToUnderdash($s) {
        $s = preg_replace('#([A-Za-z])(?=[A-Z0-9])#', '$1_', $s);
        $s = strtolower($s);
        $s = rawurlencode($s);
        return $s;
    }

    /**
     * underdash_separated -> camelCase
     *
     * @param  string
     * @return string
     */
    protected function underdashToCamel($s) {
        $s = strtolower($s);
        $s = preg_replace('#_(?=[a-z])#', ' ', $s);
        $s = substr(ucwords('x' . $s), 1);
        $s = str_replace(' ', '', $s);
        return $s;
    }

}
