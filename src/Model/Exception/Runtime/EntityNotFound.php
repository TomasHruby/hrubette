<?php

namespace Rockette\Model\Exception\Runtime;

use RuntimeException;

class EntityNotFound extends RuntimeException
{

}
