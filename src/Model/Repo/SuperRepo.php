<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */
declare(strict_types=1);

namespace Rockette\Model\Repo;

abstract class SuperRepo extends \LeanMapper\Repository
{

    const PRIMARY_KEY = 'id';

    /**
     * @param  $id
     * @return \Rockette\Model\Entity\SuperEntity|null
     * @throws \LeanMapper\Exception\InvalidStateException
     */
    public function getSingle($id) {
        // first part
        $row = $this->connection->select('*')
            ->from($this->getTable())
            ->where(self::PRIMARY_KEY . ' = %i', $id)
            ->fetch();

        if ($row === NULL) {
            return NULL;
        }
        if ($row === FALSE) {
            throw new \Rockette\Model\Exception\Runtime\EntityNotFound(sprintf('Entity with id %d returned FALSE and was not found.', $id));
        }
        // second part
        return $this->createEntity($row);
    }

    /**
     * @return array|\Rockette\Model\Entity\SuperEntity[]
     * @throws \LeanMapper\Exception\InvalidStateException
     */
    public function getAll() {
        $rows = $this->connection->select('*')
            ->from($this->getTable())
            ->fetchAll();

        if ($rows === NULL || empty($rows)) {
            return [];
        }

        return $this->createEntities($rows);
    }

    /**
     * @param  array $ids
     * @return array|\Rockette\Model\Entity\SuperEntity[]
     * @throws \LeanMapper\Exception\InvalidStateException
     */
    public function getMultiple(array $ids) {

        $ids = array_unique(array_map('intval', $ids));

        $table = $this->getTable();
        $rows = $this->connection->select('*')
            ->from("[$table]")
            ->where('[' . self::PRIMARY_KEY . '] IN %in', $ids)
            ->fetchAll();

        if ($rows === NULL || empty($rows)) {
            return [];
        }

        return $this->createEntities($rows);
    }

    /**
     * @param  array $filter
     * @return \Rockette\Model\Entity\SuperEntity|null
     * @throws \LeanMapper\Exception\InvalidStateException
     */
    public function getSingleBy(array $filter) {

        $table = $this->getTable();
        $row = $this->connection->select('*')
            ->from("[$table]")
            ->where($filter)
            ->fetch();

        if ($row === NULL) {
            return NULL;
        }
        if ($row === FALSE) {
            throw new \Rockette\Model\Exception\Runtime\EntityNotFound(sprintf('Entity with id %s returned FALSE and was not found.', $filter));
        }

        return $this->createEntity($row);
    }

}
