<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */

namespace Rockette\Model\Repo;

/**
 * @method \Rockette\Model\Entity\AccountInterface getSingle($id)
 * @method \Rockette\Model\Entity\AccountInterface[] getMultiple
 *
 * @deprecated ONLY EXAMPLE
 * @author  Tom Hruby
 * https://tomashruby.com
 * Class AccountRepo
 * @package Rockette\Model\Repo
 */
final class AccountRepo extends SuperRepo
{

}
