<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */

namespace Rockette\Model\Repo;

/**
 * @method \Rockette\Model\Entity\Address getSingle($id)
 * @method \Rockette\Model\Entity\Address[] getMultiple
 *
 * @deprecated ONLY EXAMPLE
 * @author  Tom Hruby
 * https://tomashruby.com
 * Class AddressRepo
 * @package Rockette\Model\Repo
 */
final class AddressRepo extends SuperRepo
{

}
