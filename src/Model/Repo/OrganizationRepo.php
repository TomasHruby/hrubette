<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */

namespace Rockette\Model\Repo;

/**
 * @method \Rockette\Model\Entity\Organization getSingle($id)
 * @method \Rockette\Model\Entity\Organization[] getMultiple
 *
 * @deprecated ONLY EXAMPLE
 * @author  Tom Hruby
 * https://tomashruby.com
 * Class OrganizationRepo
 * @package Rockette\Model\Repo
 */
final class OrganizationRepo extends SuperRepo
{

}
