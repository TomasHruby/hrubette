<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */

namespace Rockette\Model\Helpers;

trait TraitMetadata
{

    /**
     * @return mixed
     */
    public function getMetadataValue($param) {
        return $this->metadata[$param] ?? NULL;
    }

    /**
     * @param string $param
     * @param mixed  $value
     */
    public function setMetadataValue($param, $value) {
        $metadata = $this->metadata ?? [];
        $metadata[$param] = $value;
        $this->metadata = $metadata;
    }

    /**
     * Merges newValues into the current property metadata.
     * This is only a shallow merge - overwrites the top level metadata keys.
     *
     * @param array|null $newValues
     */
    public function mergeMetadata($newValues) {
        if (is_array($newValues)) {
            foreach ($newValues as $key => $value) {
                $this->getMetadataValue($key, $value);
            }
        }
    }

}

