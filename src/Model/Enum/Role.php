<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */
declare(strict_types=1);

namespace Rockette\Model\Enum;

class Role
{
    const ROLE_NONE = 0;

    const ROLE_ADMIN = 1;

    const ROLE_OWNER = 2;

    const ROLE_EDITOR = 3;

    const ROLE_MODERATOR = 4;

    const ROLE_GUEST = 5;

    /**
     * Lowercase
     *
     * @var string[]
     */
    public static $roleNames = [
        self::ROLE_NONE => 'none',
        self::ROLE_ADMIN => 'admin',
        self::ROLE_OWNER => 'owner',
        self::ROLE_EDITOR => 'editor',
        self::ROLE_MODERATOR => 'moderator',
        self::ROLE_GUEST => 'guest',
    ];

}
