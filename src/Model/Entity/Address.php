<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */

namespace Rockette\Model\Entity;

/**
 * @property-read int $id m:schemaPrimary
 * @property      Country $country m:hasOne m:schemaType(tinyint)
 * @property      string $city m:schemaType(varchar:127)
 * @property      string $postalCode m:schemaType(varchar:31) m:schemaComment(Postal code or ZIP)
 * @property      string $street m:schemaType(varchar:255) m:schemaComment(Street without number)
 * @property      string|null $streetPrefix m:schemaType(varchar:31) m:schemaComment(Eg. 1er, N, S, W.11)
 * @property      string|null $streetSuffix m:schemaType(varchar:31) m:schemaComment(Street Number, Eg. 273, 11E)
 * @property      string|null $houseNumber m:schemaType(varchar:15)
 * @property      string|null $apartment m:schemaType(varchar:15) m:schemaComment(Apartment number if any)
 * @property      string|null $district m:schemaType(varchar:255) m:schemaComment(District, province, municipality)
 * @property      string|null $firstname m:schemaType(varchar:127)
 * @property      string|null $midname m:schemaType(varchar:127)
 * @property      string|null $surname m:schemaType(varchar:127)
 * @property      string|null $companyName m:schemaType(varchar:255)
 * @property      string|null $contactEmail m:schemaType(varchar:255)
 * @property      string|null $contactPhone m:schemaType(varchar:31)
 * @property      string|null $latitude Coordinates
 * @property      string|null $longitude Coordinates
 * @property      string|null $note m:schemaType(varchar:1023)
 * @property      string|null $addressName m:schemaType(varchar:127)
 * @property      array|null $metadata m:passThru(jsonDecode|jsonEncode) m:schemaType(json)
 * @property      bool $active
 * @property      int $priority Priority for user defined sorting
 * @property      DateTime|null $createDate m:schemaType(DateTime)
 *
 * @deprecated ONLY EXAMPLE
 * @author  Tom Hruby
 * https://tomashruby.com
 * Class Address
 * @package Rockette\Model\Entity
 */
final class Address extends SuperEntity
{

}
