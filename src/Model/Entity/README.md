Use predefined entities only as examples. 
Inheritance of abstract entities is not recommended because of possible errors with multiple namespaces between entities. 
