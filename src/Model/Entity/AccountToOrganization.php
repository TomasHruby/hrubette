<?php
/*
* @author Tom Hruby
* https://tomashruby.com
*/

namespace Rockette\Model\Entity;

/**
 * @property-read int           $id m:schemaPrimary
 * @property      Account       $account m:hasOne(account_id:account)
 * @property      Organization  $organization m:hasOne(organization_id:organization)
 * @property      int           $role m:schemaType(tinyint) m:enum(\Rockette\Model\Enum\Role::ROLE_*) m:default(0)
 * @property      DateTime|null $createDate m:schemaType(DateTime)
 *
 * @schemaUnique account_id, organization_id
 *
 * @deprecated ONLY EXAMPLE
 * @author  Tom Hruby
 * https://tomashruby.com
 * Class AccountToOrganization
 * @package Rockette\Model\Entity
 */
final class AccountToOrganization extends SuperEntity
{

    const DEFAULT_ROLE = \Rockette\Model\Enum\Role::ROLE_NONE;

    public function initDefaults(): void {
        parent::initDefaults();
        $this->role = static::DEFAULT_ROLE;
    }

    /**
     * @return string
     */
    public function getRoleName(): string {
        return \Rockette\Model\Enum\Role::$roleNames[$this->role];
    }

}
