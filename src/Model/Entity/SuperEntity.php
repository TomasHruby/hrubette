<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */
declare(strict_types=1);

namespace Rockette\Model\Entity;

abstract class SuperEntity extends \LeanMapper\Entity
{
    use \Rockette\Model\Helpers\TraitJson;

    use \Rockette\Model\Helpers\TraitMetadata;

    const TIMEZONE_PRAGUE = 'Europe/Prague';

    const PRIORITY_VALUE = 50000;

    const ACTIVE_TRUE = TRUE;

    public function initDefaults(): void {
        parent::initDefaults();
        if (property_exists($this, 'createDate')) {
            $this->createDate = new \DateTime();
        }
        if (property_exists($this, 'priority')) {
            $this->priority = static::PRIORITY_VALUE;
        }

        if (property_exists($this, 'active')) {
            $this->active = static::ACTIVE_TRUE;
        }

    }

}
