<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */

namespace Rockette\Model\Entity;

/**
 * @property-read int           $id              m:schemaPrimary
 * @property      string|null   $firstname       m:schemaType(varchar:127)
 * @property      string|null   $midname         m:schemaType(varchar:127)
 * @property      string|null   $surname         m:schemaType(varchar:127)
 * @property      string        $username        m:schemaType(varchar:63) Unique
 * @property      string        $email           m:schemaType(varchar:255) m:schemaUnique m:schemaComment(Used for login)
 * @property      string|null   $phone           m:schemaType(varchar:31)
 * @property      string|null   $password        m:passThru(|calculateHash) m:schemaType(varchar:255) m:schemaComment(Used for login)
 * @property      int           $status          m:enum(self::STATUS_*) m:schemaType(tinyint) m:default(0)
 * @property      int|null      $timezone        m:schemaType(varchar:63)
 * @property      string|null   $country         m:schemaType(varchar:2) m:default('CZ')
 * @property      string|null   $language        m:schemaType(varchar:2) m:default('CS')
 * @property      DateTime|null $createDate      m:schemaType(DateTime)
 * @property      DateTime|null $emailVerifyDate m:schemaType(DateTime)
 * @property      DateTime|null $phoneVerifyDate m:schemaType(DateTime)
 * @property      DateTime|null $loginDate       m:schemaType(DateTime)
 * @property      array|null    $metadata        m:passThru(jsonDecode|jsonEncode) m:schemaType(json)
 *
 * @property-read AccountToOrganization[] $accountOrganizations m:belongsToMany
 * property-read Organization[] $organizations m:belongsToMany(organization_id:account_to_organization:organization_id:organization)
 *
 * @schemaUnique username
 * @schemaUnique email
 *
 * @deprecated ONLY EXAMPLE
 * @author  Tom Hruby
 * https://tomashruby.com
 * Class Account
 * @package Rockette\Model\Entity
 */
final class Account extends SuperEntity implements AccountInterface
{

    public function initDefaults(): void {
        parent::initDefaults();
        $this->createDate = new \DateTime();
        $this->status = AccountInterface::STATUS_CREATED;
        $this->timezone = self::TIMEZONE_PRAGUE;
    }

    /**
     * @return bool
     */
    public function isStatusOk(): bool {
        if ($this->status > AccountInterface::STATUS_CREATED) {
            return TRUE;
        }
        return FALSE;
    }

}
